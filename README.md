# R-Shiny

Purpose: Setting up of R-Shiny in Docker
*  R version 3.5.3
*  Hosted on port 80 instead of default 3838.

### To run:
```
docker build -t my_shiny .

docker run -d \
   -v /home/jj/my_shiny2/shiny_apps/:/srv/shiny-server/ \
   -v /home/jj/my_shiny2/shiny_logs/:/var/log/shiny-server/ \
   --name my_shiny \
   my_shiny
```

* Your shiny apps should be placed in your `shiny_apps` folder as specified in the command above.
* The logs can be seen in your `shiny_logs` folder as specified in the command above.

### To run directly from docker:
```
docker run -d \
   -v /home/jj/my_shiny2/shiny_apps/:/srv/shiny-server/ \
   -v /home/jj/my_shiny2/shiny_logs/:/var/log/shiny-server/ \
   --name my_shiny \
   jjlovesstudying/rshiny
```

### To access your app:
Open your browser and navigate to `http://your-ip`.

The IP address can be found using `docker inspect my_shiny`.
