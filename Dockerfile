FROM rocker/shiny:3.5.3   

COPY shiny-server.conf /etc/shiny-server/shiny-server.conf
EXPOSE 80

CMD ["/usr/bin/shiny-server.sh"]
